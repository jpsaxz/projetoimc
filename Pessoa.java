package com.mycompany.projeto_imc;

class Pessoa {
    private String nome; 
    private String sobrenome;
    private double peso = 0; 
    private double altura = 0; 
    
    public Pessoa(String nome, String sobrenome, double peso, double altura){
        this.nome = nome;   
        this.sobrenome = sobrenome;
        this.peso = peso;
        this.altura = altura;
    }
    
    public String getNome(){
        return nome;
    }
    public String getSobreNome(){
        return sobrenome;
    }
    public double getPeso(){
        return peso;
    }
    public double getAltura(){
        return altura;
    }
    
    public double calcularIMC(){
    
        return peso / (altura * altura);
        
    }
}