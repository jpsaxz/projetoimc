package com.mycompany.projeto_imc;

import java.text.DecimalFormat;

public class Projeto_imc {

    public static void main(String[] args) {
        
        Pessoa[] pessoas = new Pessoa[10];
        
        pessoas[0] = new Pessoa("felipe", "neves da hora", 70.6, 1.69);
        pessoas[1] = new Pessoa("sandro", "barbosa", 90.5, 1.73);
        pessoas[2] = new Pessoa("maycon", "da silva ferreira", 100.01, 1.82);
        pessoas[3] = new Pessoa("jeferson", "Oliveira", 80.04, 1.75);
        pessoas[4] = new Pessoa("manuel", "gomes vieira", 63.05, 1.8);
        pessoas[5] = new Pessoa("emanuel", "da silva", 87.5, 1.61);
        pessoas[6] = new Pessoa("isis", "santanna de deus", 59, 1.6);
        pessoas[7] = new Pessoa("maria", "santana bezerra", 63.8, 1.75);
        pessoas[8] = new Pessoa("Alessandro", "Viana", 69.53, 1.83);
        pessoas[9] = new Pessoa("sandro", "barbosa", 75, 1.54);
        
        for(int i = 0; i < pessoas.length; i++){
            Pessoa pessoa = pessoas[i];
            double imc = pessoa.calcularIMC();
            
            System.out.println(pessoa.getNome().toUpperCase() + " " + pessoa.getSobreNome().toUpperCase() + " " + new DecimalFormat(".##").format(imc));
        }
    }
}